const helper = require("../helper/helper");
const db = require("../models");
const Sequelize = require("sequelize");

module.exports = {
  addVideos: async (req, res) => {
    try {
      const newYoutubeRes = await db.youtubeVideos.create({
        // categoryId: req.body.catId,
        categoryId: 0,
        videoId: req.body.videoId,
        thumbnail: req.body.thumbnail,
        title: req.body.title,
        description: req.body.description,
      });
      return helper.success(res, "task Created", newYoutubeRes);
    } catch (error) {
      console.log(error);
    }
  },

  getRandomVideos: async (req, res) => {
    try {
      const videos = await db.youtubeVideos.findAll({
        order: Sequelize.literal("rand()"),
        raw: true,
        nest: true,
      });

      return helper.success(res, "videos list", videos);
    } catch (error) {
      console.log(error);
    }
  },
};
