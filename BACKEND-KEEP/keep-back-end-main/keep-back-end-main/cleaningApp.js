var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const db = require("./models");
var expressLayouts = require("express-ejs-layouts");
var session = require("express-session");
const basemiddleware = require("./helper/basemiddleware");
const fileUpload = require("express-fileupload");
const luxon = require('luxon');

const schedule = require('node-schedule');
const moment = require('moment')
const env = require("dotenv").config();

const PORT = Number(env.parsed.PORT) || 4006

var indexRouter = require("./routes/index");
var adminRouter = require("./routes/admin");

const helper = require('./helper/helper')

const { Expo } = require("expo-server-sdk");

var app = express();

app.use(fileUpload());

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.use(expressLayouts);
app.set("layout", "./layout/abc.ejs");
app.set("view engine", "ejs");

app.set("trust proxy", 1);
app.use(
  session({
    secret: "secret",
    resave: true,
    saveUninitialized: true,
    cookie: {
      maxAge: 24 * 60 * 60 * 365 * 1000,
    },
  })
);
app.use(basemiddleware);

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/api", indexRouter);
app.use("/admin", adminRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

app.listen(PORT, (err) => {
  if (err) {
    console.log(err, "-----------showingerrerr");
  }
  console.log("port running on ", PORT);
});

//TODO: notification Improve code down
const nowDate = new Date().toISOString();
const nowDateArr = nowDate.split("T");

// const notificationToDay = async () => {
//   const data = await db.notifications.findAll({
//     include: [
//       {
//         model: db.users,
//         as: "sender",
//         attributes: ["name", "image", "id"],
//       },
//     ],
//     raw: true,
//     nest: true,
//     where: db.sequelize.where(
//       db.sequelize.fn("DATE", db.sequelize.col("date")),
//       nowDateArr[0]
//     ),
//   });

//   return data;
// };

// const expo = new Expo();

// setInterval(async () => {
//   const day = await notificationToDay();
//   const dayN = day.map((i) => {
//     let timeNotFormated = i.time.replace(":", ".");
//     let j = parseFloat(timeNotFormated);
//     i.time = Math.round(j * 60 * 60 * 1000);
//     return i;
//   });

//   const dayOrderByHours = dayN.sort((a, b) => {
//     return a.time - b.time;
//   });

//   dayOrderByHours.map((i) => {
//     var currentDate = new Date();

//     var hours = currentDate.getHours();
//     var minutes = currentDate.getMinutes();
//     var seconds = currentDate.getSeconds();

//     var milliseconds =
//       hours * 60 * 60 * 1000 + minutes * 60 * 1000 + seconds * 1000;

//     if (milliseconds > i.time) {
//       console.log("passou");
//       return;
//     }

//     setTimeout(async () => {
//       await expo.sendPushNotificationsAsync([
//         {
//           to: "ExponentPushToken[PEk71nLBCAoGicY0aKlfT1]",
//           sound: "default",
//           body: "This is a test notification",
//         },
//       ]);
//     }, milliseconds - i.time);
//   });
// }, 1000);

const notificationToDay = async () => {
  const data = await db.notifications.findAll({
    include: [
      {
        model: db.users,
        as: "receiver",
        attributes: ["name", "image", "id", "deviceToken"], // Adicione o campo timezone do usuário
      },
    ],
    raw: true,
    nest: true,
    where: db.sequelize.where(
      db.sequelize.fn("DATE", db.sequelize.col("date")),
      nowDateArr[0]
    ),
  });
  return data;
};

const expo = new Expo();

const cronSchedule = "*/10 * * * * *";
// const cronSchedule = "* * * * *";//per min
// const cronSchedule = "* * * * * *";

//cron run every second
// const cronSchedule = "* * * * * *";
// Schedule the cron job
// const job = schedule.scheduleJob(cronSchedule, async function () {
//   const date = new Date();
//   const currentTime = moment(date).tz("America/New_York").format("HH:mm");
//   let formattedTime;
//   const hour = parseInt(currentTime);
//   if (hour >= 13 && hour <= 23) {
//     formattedTime = hour - 12 + " PM";
//   } else if (hour === 0) {
//     formattedTime = "12 AM";
//   } else if (hour === 12) {
//     formattedTime = "12 PM";
//   } else {
//     formattedTime = hour + "AM";
//   }
//   let dates = moment();
//   let d = dates.format("YYYY-MM-DD");
//   console.log("dates", dates);
//   console.log("d", d);

//   // const currentTime = moment(date).format("HH:mm:ss");
//   const currentDay = moment(date).format("dddd").toLowerCase();
//   // 726d518a79448884eb6768ef07096a5fd11f365dbbbbc350f69e82845d77827d
//   const receiverInfo = await db.users.findOne({
//     where: { id: 37 },
//     raw: true,
//     nest: true,
//   });
//   // bd17f1fb7d6714f6c9750fadfd422771a608b36e3f027704a35bbc6b83c1bb72
//   // 726d518a79448884eb6768ef07096a5fd11f365dbbbbc350f69e82845d77827d
//   // const notifyObj = {
//   //   receiverDeviceToken: "61544bde5a534b0a8bcdad3dc07d75ac8da642742dea8521021046c71797dd04",
//   //   message: "message",
//   //   taskId: 1,
//   //   day:"Monday",
//   //   time:'2PM',
//   //   type: receiverInfo.deviceType,
//   //   senderName: "Cleaning App",
//   //   senderImg: "/assets/img/logo1.png",
//   // };
//   // const result = await helper.sendPushToIos(notifyObj);

//   // return
//   try {
//     const notification = await db.notifications.findAll({
//       where: {
//         date: {
//           [db.Sequelize.Op.gte]: moment(d).startOf("day").toDate(),
//           [db.Sequelize.Op.lte]: moment(d).endOf("day").toDate(),
//         },
//         isSent: 0 || false,
//       },
//       raw: true,
//       nest: true,
//     });

//     for (let data of notification) {
//       // console.log("data=====>",data)
//       // const currentTime = moment().tz(data.timeZone).format('HH:mm');
//       let datas = data.timeZone;

//       // console.log("timezone one ",moment().tz(datas).format('HH:mm'))
//       let currentTimeInTimeZone = moment().tz(datas).format("HH:mm");
//       // console.log("USA time zone---->",moment().tz('USA').format('HH:mm'))
//       // console.log(" let data of notification", data);
//       // const timeMatches = data.time === currentTime;
//       const timeMatches = data.time === currentTimeInTimeZone;
//       // const timeMatches = data.time === formattedTime;

//       // Check if the 'day' field is null or has values
//       // const daysArray = data.day ? data.day.toLowerCase().split(",") : [];
//       const daysArray = data.day
//         ? data.day
//             .toLowerCase()
//             .split(",")
//             .map((day) => day.trim())
//         : [];
//       console.log(daysArray)
//       const dayMatches =
//         daysArray.length === 0 || daysArray.includes(currentDay);
//       if (timeMatches && dayMatches) {
//         const receiverInfo = await db.users.findOne({
//           where: { id: data.receiverId },
//           raw: true,
//           nest: true,
//         });
//         const notifyObj = {
//           receiverDeviceToken: receiverInfo.deviceToken,
//           message: data.message,
//           taskId: data.taskId,
//           day: data.day,
//           time: data.time,
//           type: receiverInfo.deviceType,
//           senderName: "Cleaning App",
//           senderImg: "/assets/img/logo1.png",
//         };

//         // const task = await db.tasks.update(
//         //   {
//         //     taskType: 0,
//         //   },
//         //   {
//         //     where: {
//         //       id: data.taskId,
//         //     },
//         //   }
//         // );
//         const result = await helper.sendPushToIos(notifyObj);
//         if (result) {
//           await db.notifications.update(
//             { isSent: true },
//             { where: { id: data.id } }
//           );
//         }
//       } else {
//         console.log("inside the else");
//       }
//     }
//   } catch (error) {
//     console.error("Error:", error);
//   }
// });


// setInterval(async () => {
//   console.log(luxon.DateTime.fromISO("2017-05-15T09:10:23").zoneName)
//   const notificationsToday = await notificationToDay();

//   const sortedNotifications = notificationsToday.sort((a, b) => {
//     return a.date - b.date || a.time - b.time;
//   });

//   sortedNotifications.map((notification) => {
//     const userTimezoneOffset = new Intl.DateTimeFormat("en", {
//       timeZone: notification.timeZone,
//     }).resolvedOptions().timeZone;

//     // Crie um objeto Date com a data e hora atuais ajustadas para o fuso horário do usuário
//     const currentDate = new Date(
//       new Date().toLocaleString("en", { timeZone: userTimezoneOffset })
//     );

//     var d = new Date("2020-04-13T00:00:00.000+08:00");
//     d.toLocaleString('en-US', { timeZone: 'America/New_York' })
//     console.log('NY ',d);

//     d.toLocaleString('pt-BR', { timeZone: 'America/Fortaleza' })

//     console.log('FR ',d);

//     // Ajuste para o fuso horário do usuário

//     // Converta a data e hora da notificação para um objeto Date
//     const notificationDateTime = new Date(
//       `${notification.date}T${notification.time}`
//     );

//     const userMillisecondsOffset =
//       notificationDateTime.getTimezoneOffset() * 60 * 1000;

//     // console.log(userMillisecondsOffset);

//     const adjustedNotificationTime = new Date(
//       notificationDateTime.getTime() - userMillisecondsOffset
//     );
//     // console.log(adjustedNotificationTime);
//     // // Ajuste para o fuso horário do usuário
//     // const userMillisecondsOffset = userTimezoneOffset * 60 * 60 * 1000;
//     // const notificationTime = new Date(
//     //   notification.date + userMillisecondsOffset + notification.time
//     // );
//     // console.log(notificationTime)
//     // if (currentDate > notificationTime) {
//     //   console.log("A notificação já passou.");
//     //   return;
//     // }

//     // const delayMilliseconds = notificationTime - currentDate;

//     // setTimeout(async () => {
//     //   await expo.sendPushNotificationsAsync([
//     //     {
//     //       to: "ExponentPushToken[PEk71nLBCAoGicY0aKlfT1]",
//     //       sound: "default",
//     //       body: "Esta é uma notificação de teste",
//     //     },
//     //   ]);
//     // }, delayMilliseconds);
//   });
// }, 1000);

// process.env.GOOGLE_APPLICATION_CREDENTIALS;

// initializeApp({
//   credential: cert({
//     project_id: "logical-fort-388916",
//     private_key: "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDlQi3D5fLXPok9\nk6icDBc8zgsCjH+dHneNp55C2fyp2CT3FLjiVyyG6GoWxZwKseYL1yYA1ejktpIw\ndrdvTux0r2fCXmzC9GL6PPhdPvbCoi+xbMHhrbtwRjVSm2x7Dfez8GLekf1icfKC\nqHFDNts6zbeD+o6jOI+/rr6hwy8oLcrBpkqoODGvQBK7vIpiWnSeTSyJtizQmUst\nZYZfBeIkI+IvxucYAOwt6S2FGSf4zLwFUFMWz3FMh9UybCj1YiC3F/HEsDlPpGY1\nNUYpqS6QbrH16cXCCOLk/AJOKloCNDbAPpmsDeU9IxnmjHwNRSr2ARBbporG6Llx\nKbwH0ZNbAgMBAAECggEAGZD2QpTnrmpKasQHNxPAGadOogOiDZcCtRjetMzTeOti\nP+2fKpqXHBpaDl0OKEEriuF82vX/6VWU2vO6RB70jpSrCQl3rowLZ6WZ+ugVI6Jr\nmCktO+ogb4Zc1u//On/VwkrJ89ErBSn45eo760d4Tkf3sa+k7X3WxgUKPEbkgOHY\nX2z1hnw8kscFTwMgTY4Bm50271qmEzSCSUQhdjZFgazEtDt2eTkYTzyK4CLMjqLB\n42taEq3yGusT7XPrY0Q0KNs1crPN5COARBT18wKItPh3wl2tx9gK8TgHJkcTvTFZ\nFGO8fBc5BeZfPfYTK5HRZFU3oCIOkIYNE4pW67tefQKBgQD4+wDm9yU499Svtq4D\nxnOfdbCTp82RO53Ghy6++G5CtAz2bSDE+FPdhyMJG/yVI+K7kF+ybgKgJO2YjDRi\n0SdZl5XNh8npnn9QiVlxF5llhJtzSPnli27jUywXHBtX/1tHB4ih1vmx9sZbiLt3\njSdYljCjTiMqvwWmNnNdU7I8fwKBgQDruNVhDI6retpwaqyluYq0PvK2CmXRF4cf\nE6k02a9mImftP76cRr88R+yUym4Thpk+m3ZGrA6WzaUZ1KcHDxHVQTEujoFgLKyB\nTv4JKGR1DLHa+ZjqFx4qVEl8rCPHWQewWRvwKtZEbyNhepSkiXgkXm0/Ggd5ppfv\n6w/0ZFqrJQKBgA7NHSid2KCiWwp3GbEvwv/fxzd/6kCQArkioCzKKAPfIEYmBqSu\n/o1U00hfWYykLX5o/sdkstNCXr2K2DJnrKcu08D8KYyx12+6Oo72kC/wyiz4UaLL\nvzPUZfKZwSZiD9MRJ5Y3aXCi8vfOERbyVTx28T1ED+Mip/tRkVoi952RAoGBAMin\nRuPc15l683TcKWjM/xgduWdihHcvavL2PoPhhaAQS502NOA8jM8YfnfNtVPjsJEN\nrunLzoNqPE83qJKmrkx0uRPjdJfUj2dnQXG0M4c7ri4zZ4pk8WWg6Ak7Ux1/WL8x\nq0mOfWQ1os14lg1Vg7KZuq8LTi+sswskcilcP54NAoGAdGkDJUSOiRY7MAHhVoKZ\nuQPNkzXwBIVRgmiXfeQgH2t24cc78Ap5vyafcKIlD4bWDlLOwP95DhP8POpWD40Y\nscL8R6GBdx48geK2s7YAekn9Bdg86GufXOcqyAwkB4LqixjQYlquPrDsSFBH11y+\nMiwKvjzGYUGsjlLVS1On5xk=\n-----END PRIVATE KEY-----\n",
//     client_email: "firebase-adminsdk-d7efq@logical-fort-388916.iam.gserviceaccount.com",
//   })
// })

// setInterval(async () => {
//   const day = await notificationToDay()
//   const dayN = day.map((i) => {
//     let timeNotFormated = i.dataValues.time.replace(":", ".")
//     let j = parseFloat(timeNotFormated)
//     i.dataValues.time = Math.round(j * 60 * 60 * 1000)
//     return i.dataValues
//   })

//   const dayOrderByHours = dayN.sort((a, b) => {
//     return a.dataValues.time - b.dataValues.time
//   })

//   dayOrderByHours.map((i) => {
//     const message = {
//       notification: {
//         title: "keep clening",
//         body: i.message
//       },
//       token: i.fireToken
//     };

//     var currentDate = new Date();

//     var hours = currentDate.getHours();
//     var minutes = currentDate.getMinutes();
//     var seconds = currentDate.getSeconds();

//     var milliseconds = hours * 60 * 60 * 1000 + minutes * 60 * 1000 + seconds * 1000;

//     if (milliseconds > i.time) {
//       console.log("passou");
//       return;
//     }

//     setTimeout(() => {
//       getMessaging()
//         .send(message)
//         .then((response) => {
//           console.log("Successfully sent message:", response);
//         })
//         .catch((error) => {
//           console.log("Error sending message:", error);
//         });
//     }, milliseconds - i.time)

//   })
// }, 1000 * 60 * 2)

module.exports = app;

// AC44d298119cdcad52305ce6d5c70db1f4
//a023e536ed72d35ff0a6e8a3544f40a4
