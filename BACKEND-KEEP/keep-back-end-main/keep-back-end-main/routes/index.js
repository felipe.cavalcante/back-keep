var express = require("express");
var router = express.Router();
const api = require("../controllers/api/authApi");
const youtubeApi = require("../controllers/youtubeVideoController");

const authenticateJWT = require("../helper/nextHelpers").authenticateJWT;
const authenticateHeader = require("../helper/nextHelpers").authenticateHeader;

router.get("/", (req,res) => {
  return res.json({
    name: "Keep Cleans",
    success: true,
    version: 1.0
  })
})

router.post("/signup", authenticateHeader, api.signup);
router.post("/login", authenticateHeader, api.login);
router.post("/socialLogin", authenticateHeader, api.socialLogin);

router.get("/getProfile", authenticateJWT, authenticateHeader, api.getProfile);
router.get("/getCmsList", authenticateHeader, api.getCmsList);
router.post("/logout", authenticateHeader, authenticateJWT, api.logOut);
router.get(
  "/deleteAccount",
  authenticateHeader,
  authenticateJWT,
  api.deleteAccount
);
router.post(
  "/changePass",
  authenticateHeader,
  authenticateJWT,
  api.changePassword
);

router.post("/forgotPassword", api.forgotPassword);
router.get("/resetPassword/:id/:ran_token", api.resetPassword);
router.post("/updateForgotPassword", api.updateForgetPassword);
router.get("/success", api.successMsg);
router.get("/linkExpired", api.linkExpired);
// router.post("/resendOtp", api.resendOtp);
router.post('/saveNotificationToken', api.saveNotification),
router.post("/contactUs", authenticateHeader, api.contactUs);
router.post(
  "/NotificationStatus",
  authenticateHeader,
  authenticateJWT,
  api.NotificationStatus
);

router.get(
  "/notificationsList",
  authenticateHeader,
  authenticateJWT,
  api.notificationsList
);
router.get(
  "/clearNotifications",
  authenticateHeader,
  authenticateJWT,
  api.clearAllNotifications
);

router.post(
  "/editProfile",
  authenticateJWT,
  authenticateHeader,
  api.editProfile
);
router.get(
  "/getVideos",
  authenticateJWT,
  authenticateHeader,
  api.getRandomVideos
);
router.get(
  "/getYoutubeVideos",
  authenticateJWT,
  authenticateHeader,
  youtubeApi.getRandomVideos
);
router.post(
  "/addYoutubeVideo",
  authenticateJWT,
  authenticateHeader,
  youtubeApi.addVideos
);
router.get(
  "/getCategories",
  authenticateJWT,
  authenticateHeader,
  api.getCategoryList
);
router.get(
  "/getRoomCategories/:id",
  authenticateJWT,
  authenticateHeader,
  api.getRoomCat
);
router.get(
  "/taskFilter/:id",
  authenticateJWT,
  authenticateHeader,
  api.taskFilter
);
router.get(
  "/getTaskDetails/:id",
  authenticateJWT,
  authenticateHeader,
  api.getTaskDetails
);
router.post("/createTask", authenticateJWT, api.createTask);

router.post(
  "/createCustomTask",
  authenticateJWT,
  authenticateHeader,
  api.createCustomTask
);
router.post("/deleteTask/:taskId", authenticateJWT, authenticateHeader, api.deleteTask);
router.post("/updateTask", authenticateJWT, authenticateHeader, api.updateTask);
router.post("/postponeTask", authenticateJWT, authenticateHeader, api.postponeTask);
router.post(
  "/isTaskCompleted",
  authenticateJWT,
  authenticateHeader,
  api.isTaskCompleted
);

router.post(
  "/updateCustomTask",
  authenticateJWT,
  authenticateHeader,
  api.updateCustomTask
);

router.get("/tasksList", authenticateJWT, authenticateHeader, api.getTasksList);
router.get(
  "/getTaskList",
  authenticateJWT,
  authenticateHeader,
  api.getTaskList
);
// router.get("/contactUs", authenticateJWT, authenticateHeader, api.contactUs);
router.get("/testFcm", authenticateJWT, authenticateHeader, api.testFcm);
router.get("/test", api.test);
router.get("/getSubscription",authenticateJWT, authenticateHeader,api.getSubscription);
module.exports = router;
