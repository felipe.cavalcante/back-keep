const Sequelize = require("sequelize");
module.exports = function (sequelize, DataTypes) {
  const youtubeVideos = sequelize.define(
    "youtubeVideos",
    {
      id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      title: {
        type: DataTypes.STRING(255),
        allowNull: false,
      },
      description: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      videoId: {
        type: DataTypes.STRING(255),
        allowNull: false,
      },
      thumbnail: {
        type: DataTypes.STRING(255),
        allowNull: true,
      },
      categoryId: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
    },
    {
      sequelize,
      tableName: "youtubeVideos",
      timestamps: true,
      indexes: [
        {
          name: "PRIMARY",
          unique: true,
          using: "BTREE",
          fields: [{ name: "id" }],
        },
      ],
    }
  );
  youtubeVideos.associate = function (models) {
    // associations can be defined here
    youtubeVideos.hasOne(models.category, {
      foreignKey: "id",
      sourceKey: "categoryId",
    });
  };
  return youtubeVideos;
};
